# imports
import pygame
import math
import random
import configparser
from ast import literal_eval
# constants
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    K_RETURN,
    K_w,
    K_a,
    K_s,
    K_d,
    QUIT,
)

# some defaults to load constants if config file does not work
LAND_HEIGHT = 40
WATER_HEIGHT = 60
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 640
MAX_FIXED_OBSTACLE = 3
MAX_MOVE_OBSTACLE = 3
SUBMARINE_SPEED = 5.0
PLAYER_SPEED = 5
SUBMARINE_HEIGHT = 39
SUBMARINE_WIDTH = 39
MAIN_FONT = "freesansbold.ttf"
black = (0, 0, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 0)
brown = (150, 75, 0)
white = (255, 255, 255)
yellow = (200, 200, 0)
NORMAL_TEXT_SIZE = 25
LARGE_TEXT_SIZE = 32
PLAYER_SIZE = 30
COIN_SIZE = 25
COIN_VALUE = 25
EXPLOSION_SIZE = 100
FIXED_OBS_REWARD = 5
MOVING_OBS_REWARD = 10

end_message = "Press Enter to start again!"


# functions
# displays Died / Cleared the round message
def displayMessage():
    screen.fill(white)
    dead_text = font.render("Player " + str(user + 1) +
                            " has cleared the round", True, red)
    if dead[user]:
        dead_text = font.render("Player {0} has died".format(str(user + 1)),
                                True, red)

    print_text(dead_text, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, True)
    time = pygame.time.get_ticks()
    pygame.display.flip()
    while pygame.time.get_ticks() - time <= 1000:
        pass


# plays background music
def play_bg_music():
    if pygame.mixer.music.get_busy():
        pygame.mixer.music.stop()
    pygame.mixer.music.load('music/NCM.wav')
    pygame.mixer.music.play(-1)


# create coins in water
def create_coins():
    for water in waters:
        water.generate_coin()


# creates explosion when player collides with obstacles
def explode():
    global exploding, ex
    exploding = True
    crash_sound = pygame.mixer.Sound("music/explosion.wav")
    pygame.mixer.Channel(1).play(crash_sound)
    x = player.rect.x
    y = player.rect.y
    player.kill()
    ex = Explosion(x, y)


# loads data from config.cfg file
def load_config():
    print("loading config")
    config = configparser.ConfigParser()
    config.read("config.cfg")
    global black, red, blue, green, brown, yellow, white
    global LAND_HEIGHT, WATER_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT, \
        MAX_FIXED_OBSTACLE, SUBMARINE_SPEED
    global PLAYER_SPEED, SUBMARINE_HEIGHT, SUBMARINE_WIDTH, \
        MAX_MOVE_OBSTACLE, MAIN_FONT
    global NORMAL_TEXT_SIZE, LARGE_TEXT_SIZE, end_message, PLAYER_SIZE, \
        COIN_VALUE, COIN_SIZE, EXPLOSION_SIZE
    global FIXED_OBS_REWARD, MOVING_OBS_REWARD
    colors = config['Colors']
    black = literal_eval(colors['black'])
    red = literal_eval(colors['red'])
    green = literal_eval(colors['green'])
    brown = literal_eval(colors['brown'])
    yellow = literal_eval(colors['yellow'])
    white = literal_eval(colors['white'])

    settings = config['Screen Settings']
    LAND_HEIGHT = int(settings['LAND_HEIGHT'])
    WATER_HEIGHT = int(settings['WATER_HEIGHT'])
    SCREEN_WIDTH = int(settings['SCREEN_WIDTH'])
    SCREEN_HEIGHT = int(settings['SCREEN_HEIGHT'])

    ob = config['Obstacles']
    MAX_FIXED_OBSTACLE = int(ob['MAX_FIXED_OBSTACLE'])
    MAX_MOVE_OBSTACLE = int(ob['MAX_MOVE_OBSTACLE'])
    SUBMARINE_SPEED = float(ob['SUBMARINE_SPEED'])
    SUBMARINE_HEIGHT = int(ob['SUBMARINE_HEIGHT'])
    SUBMARINE_WIDTH = int(ob['SUBMARINE_WIDTH'])
    COIN_SIZE = int(ob['COIN_SIZE'])
    COIN_VALUE = int(ob['COIN_VALUE'])
    EXPLOSION_SIZE = int(ob['EXPLOSION_SIZE'])
    FIXED_OBS_REWARD = int(ob['FIXED_OBS_REWARD'])
    MOVING_OBS_REWARD = int(ob['MOVING_OBS_REWARD'])

    pl = config['Player']
    PLAYER_SPEED = int(pl['PLAYER_SPEED'])
    PLAYER_SIZE = int(pl['PLAYER_SIZE'])
    f = config['Fonts']
    MAIN_FONT = f['MAIN_FONT']
    NORMAL_TEXT_SIZE = int(f['NORMAL_TEXT_SIZE'])
    LARGE_TEXT_SIZE = int(f['LARGE_TEXT_SIZE'])

    s = config['Messages']
    end_message = s['end_message']
    print("config loaded successfully")


# adds time bonus to current players score
def add_time_bonus():
    print("score + timer")
    score[user] += player.timer


# displays the time bonus on screen
def add_timer():
    t_font = pygame.font.Font(MAIN_FONT, NORMAL_TEXT_SIZE)
    timer_text = t_font.render("Bonus: " + str(player.timer), True, yellow)
    print_text(timer_text, SCREEN_WIDTH - 200,
               SCREEN_HEIGHT - timer_text.get_rect().h)


# displays the end-screen when both players die
def end_screen():
    screen.fill(white)
    font2 = pygame.font.Font(MAIN_FONT, LARGE_TEXT_SIZE)
    winner_text = font2.render("Player " + str(winner) +
                               " has won the game by " +
                               str(abs(score[0] - score[1])) +
                               " points", True, red, white)
    random_text = font.render(end_message, True, red, white)
    print_text(winner_text, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, True)
    print_text(random_text, (SCREEN_WIDTH - random_text.get_rect().w) / 2,
               SCREEN_HEIGHT - 10 - random_text.get_rect().h)
    player.kill()
    for land in lands:
        land.kill()
    for water in waters:
        water.kill()
    for ob in obs:
        ob.kill()
    for mob in mov_obs:
        mob.kill()


# used to render player name, speed, start, end, level
def render_text():
    # score text
    score_text = font.render("P1 =" + str(score[0]) +
                             " P2 = " + str(score[1]), True, yellow)
    print_text(score_text, 10, 10)
    # which player text
    player_text = font.render("Player 1" if user == 0
                              else "Player 2", True, yellow)
    print_text(player_text, (SCREEN_WIDTH - player_text.get_rect().w) / 2, 10)
    # render start and end
    start = "start"
    end = "end"
    if user == 1:
        start = "end"
        end = "start"
    s = font.render(start, True, yellow)
    e = font.render(end, True, yellow)
    print_text(s, SCREEN_WIDTH - s.get_rect().w - 5,
               SCREEN_HEIGHT - s.get_rect().h - 10)
    print_text(e, SCREEN_WIDTH - e.get_rect().w - 5, 0 + 10)
    render_level()


# a helper function to quickly print text on screen
def print_text(rendered_text, x_cord, y_cord, centre=False):
    rendered_rect = rendered_text.get_rect()
    rendered_rect.x = x_cord
    rendered_rect.y = y_cord
    if centre:
        screen.blit(rendered_text,
                    rendered_text.get_rect(center=(x_cord, y_cord)))
    else:
        screen.blit(rendered_text, rendered_rect)


# calculate and render submarine speed aka level
def render_level():
    level_text = font.render("Submarine Speed : " +
                             str(curLevel) + "X", True, yellow)
    level_rect = level_text.get_rect()
    level_rect.x = 0
    level_rect.y = SCREEN_HEIGHT - level_rect.h - 10
    screen.blit(level_text, level_rect)


# resets some parameters when player dies or completes a level
def reset():
    global user, player
    if not dead[1 - user]:
        user = 1 - user
    elif not dead[user]:
        user = user
    elif dead[1 - user]:
        compare_scores()
        return
    player = Player()
    for coin in coins:
        coin.kill()
    for water in waters:
        water.generate_coin()
    turn[user] += 1
    print(turn)
    x = 1 + float(turn[user]) * 0.25
    for a_moving_obs in mov_obs:
        a_moving_obs.speed = a_moving_obs.base_speed * x
    global curLevel
    curLevel = x
    render_level()
    print("current speed is " + str(SUBMARINE_SPEED))
    # reset everything
    for landd in lands:
        landd.collided = False
    for waterr in waters:
        waterr.collided = False


# constructs the land, water, obstacles, coins
def make_land_and_water():
    pairs = math.floor(SCREEN_HEIGHT / (LAND_HEIGHT + WATER_HEIGHT))
    extra_land = SCREEN_HEIGHT % (LAND_HEIGHT + WATER_HEIGHT)
    y = 0
    counter = 0
    for i in range(pairs):
        # make land
        counter += 1
        land = Land(LAND_HEIGHT, y, counter)
        land.create_obstacle()
        y += LAND_HEIGHT
        # add water
        counter += 1
        water = Water(WATER_HEIGHT, y, counter)
        water.create_obstacle()
        y += WATER_HEIGHT
        lands.add(land)
        waters.add(water)

    counter += 1
    global last_land
    last_land = counter
    land = Land(extra_land, y, counter)
    land.create_obstacle()
    lands.add(land)


# checks score when both player die
def compare_scores():
    print("compairing")
    global winner
    winner = 2
    if score[0] > score[1]:
        winner = 1
    global game_over
    game_over = True


# used to initilialize the global variables
def init():
    global last_land, score, dead, user, game_over, winner, \
        turn, curLevel, player, \
        lands, waters, obs, mov_obs, clock, font
    last_land = 0
    score = [0, 0]
    dead = [False, False]
    user = 0
    game_over = False
    winner = 2
    turn = [0, -1]
    curLevel = 1.0
    player = Player()
    all_sprites = pygame.sprite.Group()
    lands = pygame.sprite.Group()
    waters = pygame.sprite.Group()
    obs = pygame.sprite.Group()
    mov_obs = pygame.sprite.Group()
    clock = pygame.time.Clock()
    all_sprites.add(player)
    font = pygame.font.Font('freesansbold.ttf', 20)
    pygame.time.set_timer(DECREASE_TIMER, 1000)
    play_bg_music()
    make_land_and_water()
    create_coins()


# classes
# this class is the player object
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.rect = pygame.Rect(SCREEN_WIDTH / 2 - PLAYER_SIZE / 2,
                                0, PLAYER_SIZE, PLAYER_SIZE)
        self.timer = 50
        if user == 0:
            self.rect.y = SCREEN_HEIGHT - 20
            self.image = pygame.image.load("images/ghost2.png")
        if user == 1:
            self.rect.y = 0
            self.image = pygame.image.load("images/ghost.png")
        self.speed = PLAYER_SPEED
        self.movement = False
        self.image = pygame.transform.scale(self.image, (30, 30))

    def update(self, p_k):
        if user == 0:
            if p_k[K_UP]:
                self.rect.move_ip(0, -self.speed)
                self.movement = True
            if p_k[K_DOWN]:
                self.rect.move_ip(0, self.speed)
            if p_k[K_LEFT]:
                self.rect.move_ip(-self.speed, 0)
                self.movement = True
            if p_k[K_RIGHT]:
                self.rect.move_ip(self.speed, 0)
                self.movement = True

        elif user == 1:
            if p_k[K_w]:
                self.rect.move_ip(0, -self.speed)
                self.movement = True
            if p_k[K_s]:
                self.rect.move_ip(0, self.speed)
                self.movement = True
            if p_k[K_a]:
                self.rect.move_ip(-self.speed, 0)
                self.movement = True
            if p_k[K_d]:
                self.rect.move_ip(self.speed, 0)
                self.movement = True
        if self.speed == 0:
            return
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

    def decrement_timer(self):
        if self.movement:
            self.timer -= 1

    def kill(self):
        super(Player, self).kill()
        self.speed = 0
        self.rect.x = 10000


# This class is modelling lands
class Land(pygame.sprite.Sprite):
    def __init__(self, height, y_cord, l_number):
        super(Land, self).__init__()
        self.rect = pygame.Rect(0, y_cord, SCREEN_WIDTH, height)
        s = pygame.image.load("images/grass.jpg")
        self.image = pygame.transform.scale(s, (SCREEN_WIDTH, height))
        self.l_number = l_number
        self.collided = False
        self.points = 0

    def create_obstacle(self):
        if self.l_number == 1 or self.l_number == last_land:
            self.points = 0
            return
        number = random.randint(2, MAX_FIXED_OBSTACLE)
        self.points = FIXED_OBS_REWARD * number
        for i in range(number):
            obstacle = FixedObstacle(self.rect.y)
            obs.add(obstacle)
        print("created")

    def on_collision(self):
        if self.collided:
            return
        self.collided = True
        print(self.l_number)
        if self.l_number == 1 and user == 1:
            print("initial p2")
            return
        elif self.l_number == last_land and user == 0:
            print("inital p1")
            return
        else:
            for water in waters:
                if water.l_number == self.l_number + (1 if user == 0 else -1):
                    global score
                    score[user] += water.points
            if self.l_number == last_land and user == 1:
                print("finally the end")
                add_time_bonus()
                displayMessage()
                reset()
            elif self.l_number == 1 and user == 0:
                print("finally again end")
                add_time_bonus()
                displayMessage()
                reset()


# This class models the fixed obstacles
class FixedObstacle(pygame.sprite.Sprite):
    def __init__(self, height):
        super(FixedObstacle, self).__init__()
        self.rect = pygame.Rect(random.randint(0,
                                               SCREEN_WIDTH - LAND_HEIGHT),
                                height, LAND_HEIGHT - 6, LAND_HEIGHT)
        self.image = pygame.image.load("images/fire.png")
        self.image = pygame.transform.scale(self.image,
                                            (LAND_HEIGHT - 6, LAND_HEIGHT))


# This class models the Water
class Water(pygame.sprite.Sprite):
    def __init__(self, height, y_cord, w_number):
        super(Water, self).__init__()
        # self.surf = pygame.Surface((SCREEN_WIDTH, height))
        # self.surf.fill(blue)
        # self.rect = self.surf.get_rect()
        # self.rect.y = y_cord
        self.rect = pygame.Rect(0, y_cord, SCREEN_WIDTH, height)
        s = pygame.image.load("images/water.jpg")
        self.image = pygame.transform.scale(s, (SCREEN_WIDTH, height))
        self.l_number = w_number
        self.collided = False
        self.points = 0

    def create_obstacle(self):
        number = random.randint(2, MAX_MOVE_OBSTACLE)
        self.points = MOVING_OBS_REWARD * number
        for i in range(number):
            obstacle = MovingObstacle(self.rect.y)
            mov_obs.add(obstacle)
        print("created")

    def on_collision(self):
        if self.collided:
            return
        self.collided = True
        print(self.l_number)
        for land in lands:
            if land.l_number == self.l_number + (1 if user == 0 else -1):
                global score
                score[user] += land.points

    def generate_coin(self):
        x = random.uniform(0, 1)
        if x > 0.35:
            coin = Coin(self.rect.y)
            coins.add(coin)


# This class models moving obstacles
class MovingObstacle(pygame.sprite.Sprite):
    def __init__(self, height):
        super(MovingObstacle, self).__init__()
        self.rect = pygame.Rect(random.randint(0,
                                               SCREEN_WIDTH - SUBMARINE_WIDTH),
                                height +
                                (WATER_HEIGHT - SUBMARINE_HEIGHT) / 2,
                                SUBMARINE_WIDTH, SUBMARINE_HEIGHT)
        self.image = pygame.image.load("images/submarine3.png")
        self.speed = random.uniform(1, SUBMARINE_SPEED)
        self.base_speed = self.speed

    def update(self):
        self.rect.move_ip(self.speed, 0)
        if self.rect.left > SCREEN_WIDTH:
            self.rect.x = -WATER_HEIGHT


# This class models the coins in the river
class Coin(pygame.sprite.Sprite):
    def __init__(self, height):
        super(Coin, self).__init__()
        self.rect = pygame.Rect(random.randint(0, SCREEN_WIDTH - COIN_SIZE),
                                height +
                                (WATER_HEIGHT - COIN_SIZE) / 2,
                                COIN_SIZE, COIN_SIZE)
        self.index = 0
        self.images = []
        for i in range(1, 10):
            self.images.append(pygame.image.load("./images/goldCoin/goldCoin" +
                                                 str(i) + ".png"))
        print("here")
        self.image = self.images[self.index]
        self.slow = 3

    def update(self):
        self.slow += 1
        self.slow %= 5
        if self.slow == 0:
            self.index = (self.index + 1) % 8 + 1
            self.image = self.images[self.index]


# This class models the explosion on death
class Explosion(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Explosion, self).__init__()
        self.rect = pygame.Rect(x - EXPLOSION_SIZE / 2, y - EXPLOSION_SIZE / 2,
                                EXPLOSION_SIZE, EXPLOSION_SIZE)
        self.index = 0
        self.images = []
        for i in range(0, 9):
            s = pygame.image.load("./images/explosion/tile00" +
                                  str(i) + ".png")
            self.images.append(s)
        print("here")
        self.image = self.images[self.index]
        self.slow = 3
        player.kill()

    def update(self):

        self.slow += 1
        self.slow %= 5
        if self.slow == 0:
            self.index = (self.index + 1) % 9
            self.image = self.images[self.index]
        if self.index == 8:
            global exploding
            displayMessage()
            play_bg_music()
            exploding = False
            reset()


# setup - declaring globals and assigning defaults
pygame.init()
last_land = 0
score = [0, 0]
dead = [False, False]
user = 0
game_over = False
winner = 2
turn = [0, -1]
curLevel = 1.0
DECREASE_TIMER = pygame.USEREVENT + 1
player = Player()
lands = pygame.sprite.Group()
waters = pygame.sprite.Group()
obs = pygame.sprite.Group()
mov_obs = pygame.sprite.Group()
clock = pygame.time.Clock()
coins = pygame.sprite.Group()
ex = Explosion(0, 0)
font = pygame.font.Font(MAIN_FONT, NORMAL_TEXT_SIZE)
load_config()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
init()
exploding = False
# loop control variable
running = True

# game loop
while running:
    # event handling
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
            if event.key == K_RETURN and game_over:
                init()
        elif event.type == QUIT:
            running = False
        elif event.type == DECREASE_TIMER:
            player.decrement_timer()
    # move player
    pressed_keys = pygame.key.get_pressed()
    player.update(pressed_keys)
    # background
    screen.fill(black)
    # render everything
    for a_land in lands:
        screen.blit(a_land.image, a_land.rect)
    for a_water in waters:
        screen.blit(a_water.image, a_water.rect)
    for a_obstacle in obs:
        screen.blit(a_obstacle.image, a_obstacle.rect)
    for a_obstacle in mov_obs:
        screen.blit(a_obstacle.image, a_obstacle.rect)
        a_obstacle.update()
    for a_coin in coins:
        screen.blit(a_coin.image, a_coin.rect)
        a_coin.update()
    render_text()
    add_timer()
    if not exploding:
        screen.blit(player.image, player.rect)
    if exploding:
        screen.blit(ex.image, ex.rect)
        ex.update()

    # check for collisions
    if pygame.sprite.spritecollideany(player, lands):
        collided_land = pygame.sprite.spritecollideany(player, lands)
        collided_land.on_collision()
    if pygame.sprite.spritecollideany(player, waters):
        collided_water = pygame.sprite.spritecollideany(player, waters)
        collided_water.on_collision()
    if pygame.sprite.spritecollideany(player, coins):
        pygame.sprite.spritecollideany(player, coins).kill()
        coin_sound = pygame.mixer.Sound("music/coin.wav")
        pygame.mixer.Channel(1).play(coin_sound)
        score[user] += COIN_VALUE
    if not exploding:
        if pygame.sprite.spritecollideany(player, obs) or \
                pygame.sprite.spritecollideany(player, mov_obs):
            dead[user] = True
            add_time_bonus()
            explode()
    if game_over:
        end_screen()
    pygame.display.flip()
    # set fps to 30
    clock.tick(30)

pygame.quit()
